<?php
include_once ($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "BITM_Atomic_Project" . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . "startup.php");

use App\BITM\SEIP107348\ProfilePicture\ProfilePicture;
use App\BITM\SEIP107348\Utility\Utility;

$profile = new ProfilePicture();

$profiles = $profile->index();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Profile Pictures </title>
        <link href="../../../Asset/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../Asset/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../Asset/css/main.css" rel="stylesheet" type="text/css"/>
        <script src="../../../Asset/js/jquery-1.11.3.min.js" type="text/javascript"></script>
        <script src="../../../Asset/js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
    <body>
        <section class="header_part">
            <div class="container">
                <div class="row">
                    <div>
                        <div class="col-md-6">
                            <p class="header_text text-success">THE LIST OF PICTURES</p>
                        </div>
                        <div class="col-md-6">  					

<!--                            <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-envelope"></span> yasin.faruk8585@gmail.com</a>
                            </p>-->
                            <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-hand-right"></span>  SEIP-107348, 107477, 107897, 107314</a>
                            </p>
                            <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-user"></span> Code Champs</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <!-- =============== table-section =============== -->
        <section class="table_section">
            <div class="container">
                <div class="row col-md-10 col-md-offset-1  custyle">
                    <div class="table_nav">

                        <!------------------ insert-message ----------------->
                        <div>

                        </div>
                        <div class="alert alert-dismissible alert-success alert-message">
                            <button type="button" class="close" data-dismiss="alert">&Cross;</button>
                            <h4>Message!</h4>
                            <?php echo Utility::message(); ?>
                        </div>

                        <nav class="navbar navbar-default" role="navigation">
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse navbar-ex1-collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="../../../index.php">HOME</a></li>
                                    <li><a href="#">VIEW</a></li>
                                    <li><a href="create.php">ADD PICTURE</a></li>
                                    <!--<li><a href="#">Link</a></li>-->

                                </ul>

                                <ul class="nav navbar-nav navbar-right"> 
                                    <form class="navbar-form navbar-left" role="search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Search">
                                        </div>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </form>

                                    <form class="navbar-form navbar-left" role="search" action="download.php" method="post">
                                        <div class="form-group">
                                            <input type="submit" value="Download" name="export" class="form-control" />
                                        </div>
                                    </form>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </nav>
                    </div>

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>#ID</th>
                                <th>Name &dArr;</th>
                                <th>Profile Picture &dArr;</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $slno = 1;
                            foreach ($profiles as $profile) {
                                ?>
                                <tr>
                                    <td><?php echo $slno; ?></td>
                                    <td><?php echo $profile->id; ?></td>
                                    <td><a href="show.php?id=<?php echo $profile->id; ?>"><?php echo $profile->name; ?></a></td>
                                    <td><img width = "80px" height="80px" src="upload/<?php echo $profile->profile_pic; ?>" /></td>
                                    <td class="text-center ">
                                        <a class='btn btn-success btn-xs' href="edit.php?id=<?php echo $profile->id; ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a> 
                                       <form style = "display: inline;"action="delete.php" method="post">
                                            <input type="hidden" name ="id" value="<?php echo $profile->id; ?>">
                                            <button type="submit" class="btn btn-danger btn-xs delete"><span class="glyphicon glyphicon-trash"></span>  Delete</button>
                                        </form>
                                        <a href="show.php?id=<?php echo $profile->id; ?>" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-trash"></span> View</a>
                                        
                                        
                                    </td>
                                </tr>
                                <?php
                                $slno++;
                            }
                            ?>

                        </tbody>
                    </table>
                </div>

                <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                <!-- Include all compiled plugins (below), or include individual files as needed -->
                <script src="../../js/bootstrap.min.js"></script>
                <script>
                    $('.delete').bind('click', function (e) {
                        var deleteItem = confirm("Are you sure you want to delete?");
                        if (!deleteItem) {
                            //return false; 
                            e.preventDefault();
                        }
                    });
                </script>
                </body>
                </html>