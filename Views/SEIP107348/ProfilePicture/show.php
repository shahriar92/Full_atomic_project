<?php
include_once ($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "BITM_Atomic_Project" . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . "startup.php");
    
    use App\BITM\SEIP107348\ProfilePicture\ProfilePicture;
    use App\BITM\SEIP107348\Utility\Utility;
    
    $profileItem = new ProfilePicture();
    $profile = $profileItem->show($_GET["id"]);

    
 ?>


<!DOCTYPE>
<html>
    <head>
        <title>Image Store</title>
        <link href="../../../Asset/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../Asset/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../Asset/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <section class="header_part">
            <div class="container">
                <div class="row">
                    <div>
                        <div class="col-md-6">
                            <p class="header_text text-success">THE LIST OF IMAGES</p>
                        </div>
                        <div class="col-md-6">  					

<!--                            <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-envelope"></span> yasin.faruk8585@gmail.com</a>
                            </p>-->
                            <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-hand-right"></span>  SEIP-107348, 107477, 107897, 107314</a>
                            </p>
                            <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-user"></span> Code Champs</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- =============== table-section =============== -->
        <section class="table_section">
            <div class="container">
                <div class="row col-md-10 col-md-offset-1  custyle">
                    <div class="table_nav">
                        <nav class="navbar navbar-default" role="navigation">
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse navbar-ex1-collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="../../../index.php">HOME</a></li>
                                    <li><a href="index.php"> VIEW</a></li>
                                    <li><a href="create.php">ADD FILE</a></li>
                                    <!--<li><a href="#">Link</a></li>-->

                                </ul>

                                <ul class="nav navbar-nav navbar-right"> 
                                    <form class="navbar-form navbar-left" role="search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Search">
                                        </div>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </form>

                                    <li><a href="#">Download PDF</a></li>

                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </nav>
                    </div>
                    <!-- ----------------- table ------------------ -->
                    <div class="table_part">
                        <table class="table table-bordered custab">

                          
                            
                            <tr>
                                <td colspan="3" > <p style = "text-align: center; color: #0098B3; font-weight: bold; font-size: 140%"><?php echo $profile->name; ?>'s Profile Picture</p> 
                                <img class = "img-thumbnail center-block"  height="120px" width="120px" src="upload/<?php echo $profile->profile_pic;?>">
                                <p style = "text-align: center;"></br>
                                    <a href="edit.php?id=<?php echo $profile->id; ?>"><button>Change Image</button></br></a>
                                </p>    
                                </td>
                                
                            </tr>

                  

                        </table>

                    </div>
                </div>
            </div>
        </section>
    </body>
</html>

