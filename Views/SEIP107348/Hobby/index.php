<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."BITM_Atomic_Project".DIRECTORY_SEPARATOR."Views".DIRECTORY_SEPARATOR."startup.php");

use App\BITM\SEIP107348\Hobby\Hobby;
use App\BITM\SEIP107348\Utility\Utility;

$obj = new Hobby();
$obj = $obj->index(); 

?>

<!DOCTYPE>
<html>
    <head>
        <title>Hobbies</title>
        <link href="../../../Asset/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../Asset/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../Asset/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
  
    <body>
        <section class="header_part">
            <div class="container">
                <div class="row">
                   <div>
                        <div class="col-md-2">
                            <p class="header_text text-success">Codechamps</p>
                        </div>
                        <div class="col-md-10">  					

                             <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-hand-right"></span>  SEIP-107348, 107477, 107897, 107314</a>
                            </p>
                            <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-user"></span> Code Champs</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>          
        <!-- =============== navbar-section =============== -->
        <section class="table_section">          
            <div class="container">
                                <div class="row col-md-10 col-md-offset-1  custyle">
                                    <div id ="message" > <?php echo Utility::Message()     ?> </div>
                                </div>
            </div>
        </section>
        <section class="table_section">
            <div class="container">
                <div class="row col-md-10 col-md-offset-1  custyle">
                    <div class="table_nav">
                        <nav class="navbar navbar-default" role="navigation">
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse navbar-ex1-collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="../../../index.php">HOME</a></li>
                                    <li><a href="index.php">VIEW</a></li>
                                    <li><a href="create.php">ADD Hobbies</a></li>
                                    <!--<li><a href="#">Link</a></li>-->
                                </ul>

                                <ul class="nav navbar-nav navbar-right"> 
                                    <form class="navbar-form navbar-left" role="search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Search">
                                        </div>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </form>

                                    <li><a href="#">Download PDF</a></li>

                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </nav>
                    </div>

                    <!-- ----------------- table ------------------ -->
                    
                    
                    <div class="table_part">
                        <table class="table table-bordered custab">

                            <thead>
                                <!-- <a href="#" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new categories</a> -->
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Hobbies</th>

                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <?php $slno = 1; foreach($obj as $hobbies) { ?>
                            <tr>
                                <td><?php echo $slno;  ?></td>
                                <td><a href = "show.php?id=<?php echo $hobbies['id']?>"><?php echo $hobbies['name']; ?></a></td>
                                <td><?php echo  $hobbies['hobby'] ?></td>

                                <td class="text-center"><a class='btn btn-info btn-xs' href="edit.php?id=<?php echo $hobbies['id']; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="delete.php?id=<?php echo $hobbies['id'];?>" class="delete btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Delete</a>
                                
                                </td>
                            </tr>
                            <?php $slno++;  }  ?>
                        </table>

                    </div>
                    <!-- ---------- pagination ------------ -->
                    <div class="table_pagination">
                        <ul class="pagination pagination-sm">
                            <li class="disabled"><a href="#">&laquo;</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
        <script>
                $('.delete').bind('click', function(e) {
                    var deleteItem = confirm("are you sure you want to delete?"); 
                    if(!deleteItem) {
                        //return false ; 
                        e.preventDefault(); 
                        
                    }
                })
        </script>

      
    </body>
</html>
